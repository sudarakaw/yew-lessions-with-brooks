use yew::{html, Component};
use yewdux::prelude::{BasicStore, DispatchProps, Dispatcher, WithDispatchProps};

use crate::store::YdxStore;

pub struct Counter {
    // IMPORTANT: without (clone of) a context dispatch here, component state
    // does not get updated when placed in a router.
    //
    // NOTE: name "_dispatch" does not have a special meaning. And it is an
    // unused member of the struct.
    //
    _dispatch: DispatchProps<BasicStore<YdxStore>>,
}

impl Component for Counter {
    type Message = ();
    type Properties = DispatchProps<BasicStore<YdxStore>>;

    fn create(ctx: &yew::Context<Self>) -> Self {
        let _dispatch = ctx.props().dispatch().clone();

        Self { _dispatch }
    }

    fn view(&self, ctx: &yew::Context<Self>) -> yew::Html {
        let count = ctx.props().state().count;
        let on_click = ctx
            .props()
            .dispatch()
            .reduce_callback(|state| state.count += 1);

        html! {
            <div>
                <h1>{"Counter"}</h1>

                <p>{format!("The button has been pressed {} times", count)}</p>

                <button onclick={on_click}>{"Click Me!"}</button>
            </div>
        }
    }
}
