use yewdux_example::App;

fn main() {
    yew::start_app::<App>();
}
