use yew::{html, Html};
use yew_router::Routable;
use yewdux::prelude::WithDispatch;

use crate::counter::Counter;

#[derive(Routable, Clone, PartialEq)]
pub enum Route {
    #[at("/")]
    Counter,
}

pub fn switch(route: &Route) -> Html {
    match route {
        Route::Counter => html! { <WithDispatch<Counter> /> },
    }
}
