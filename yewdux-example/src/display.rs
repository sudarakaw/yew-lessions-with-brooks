use yew::{html, Component};
use yewdux::prelude::{BasicStore, DispatchProps};

use crate::store::YdxStore;

pub struct DisplayCount;

impl Component for DisplayCount {
    type Message = ();
    type Properties = DispatchProps<BasicStore<YdxStore>>;

    fn create(_ctx: &yew::Context<Self>) -> Self {
        Self
    }

    fn view(&self, _ctx: &yew::Context<Self>) -> yew::Html {
        html! {
            <div>
                <h1>{"Display Count"}</h1>
            </div>
        }
    }
}

pub struct DisplayForm;

impl Component for DisplayForm {
    type Message = ();
    type Properties = DispatchProps<BasicStore<YdxStore>>;

    fn create(_ctx: &yew::Context<Self>) -> Self {
        Self
    }

    fn view(&self, ctx: &yew::Context<Self>) -> yew::Html {
        let state = ctx.props().state();

        html! {
            <div>
                <h1>{"Display Form"}</h1>
                <p>{format!("Username: {}, Password: {}", state.username, state.password)}</p>
            </div>
        }
    }
}
