// mod counter;
mod display;
mod login;
// mod router;
mod store;

use display::DisplayForm;
// use counter::Counter;
// use display::DisplayCount;
use login::Login;
// use router::{switch, Route};
use yew::{html, Component};
// use yew_router::{BrowserRouter, Switch};
use yewdux::prelude::WithDispatch;

pub struct App;

impl Component for App {
    type Message = ();
    type Properties = ();

    fn create(_ctx: &yew::Context<Self>) -> Self {
        Self
    }

    fn view(&self, _ctx: &yew::Context<Self>) -> yew::Html {
        html! {
            <div>
                <h1>{"App"}</h1>
                <WithDispatch<Login> />
                <WithDispatch<DisplayForm> />
            </div>
        }
    }
}
