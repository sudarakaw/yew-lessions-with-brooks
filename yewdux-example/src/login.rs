use gloo::console::log;
use wasm_bindgen::JsCast;
use web_sys::HtmlInputElement;
use yew::{html, Callback, Component, Event, FocusEvent};
use yewdux::prelude::{BasicStore, DispatchProps, Dispatcher, WithDispatchProps};

use crate::store::YdxStore;

pub struct Login {
    // IMPORTANT: without (clone of) a context dispatch here, component state
    // does not get updated when placed in a router.
    //
    // NOTE: name "_dispatch" does not have a special meaning. And it is an
    // unused member of the struct.
    //
    _dispatch: DispatchProps<BasicStore<YdxStore>>,
}

impl Component for Login {
    type Message = ();
    type Properties = DispatchProps<BasicStore<YdxStore>>;

    fn create(ctx: &yew::Context<Self>) -> Self {
        let _dispatch = ctx.props().dispatch().clone();

        Self { _dispatch }
    }

    fn view(&self, ctx: &yew::Context<Self>) -> yew::Html {
        let handle_form_submit = {
            let state = ctx.props().state();

            Callback::from(move |event: FocusEvent| {
                event.prevent_default();

                let username = state.username.clone();
                let password = state.password.clone();

                log!("Username:", username);
                log!("Password:", password);
            })
        };
        let handle_username_change =
            ctx.props()
                .dispatch()
                .reduce_callback_with(|state, event: Event| {
                    let username = event
                        .target()
                        .unwrap()
                        .unchecked_into::<HtmlInputElement>()
                        .value();

                    state.username = username;
                });
        let handle_password_change =
            ctx.props()
                .dispatch()
                .reduce_callback_with(|state, event: Event| {
                    let password = event
                        .target()
                        .unwrap()
                        .unchecked_into::<HtmlInputElement>()
                        .value();

                    state.password = password;
                });

        html! {
            <form onsubmit={handle_form_submit}>
                <h1>{"Login"}</h1>

                <div>
                    <input type="text" placeholder="username" onchange={handle_username_change} />
                </div>
                <div>
                    <input type="password" placeholder="password" onchange={handle_password_change} />
                </div>
                <div>
                    <button>{"Log In"}</button>
                </div>
            </form>
        }
    }
}
