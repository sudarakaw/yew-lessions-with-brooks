mod components;
mod router;

use std::ops::Deref;

use gloo::console::log;
use stylist::yew::styled_component;
use yew::{html, use_effect, use_state, Callback, ContextProvider};
use yew_router::{BrowserRouter, Switch};

use crate::components::atoms::main_title::{Color, MainTitle};
use crate::components::molecules::custom_form::{self, CustomForm};
use crate::router::{switch, Route};

#[derive(Clone, PartialEq, Default)]
struct User {
    username: String,
    language: String,
}

#[styled_component(App)]
pub fn app() -> Html {
    let state = use_state(User::default);
    let first_load = use_state(|| true);

    let main_title_load = Callback::from(|message: String| log!(message));
    let custom_form_submit = {
        let state = state.clone();

        Callback::from(move |data: custom_form::State| {
            let mut user = state.deref().clone();

            user.username = data.username;
            user.language = data.language;

            state.set(user);
        })
    };

    use_effect(move || {
        // This code will run on
        //  - first render
        //  - all re-renders

        // Example:
        // if auth token exists & this is the first render
        //   get all information of the user from data store

        if *first_load {
            // This should only run on first load

            log!("THIS IS THE FIRST RENDER!!!!");

            // Make not e that "first load" is done.
            first_load.set(false);
        } else {
            log!("This is a subsequent render.");
        }

        || {}
    });

    html! {
        <ContextProvider<User> context={state.deref().clone()}>
            <MainTitle title="Hello from Yew!" color={Color::Ok} on_load={main_title_load} />

            <CustomForm on_submit={custom_form_submit} />

            <BrowserRouter>
                <Switch<Route> render={Switch::render(switch)} />
            </BrowserRouter>
        </ContextProvider<User>>
    }
}
