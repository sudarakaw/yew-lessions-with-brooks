use yew::{html, Html};
use yew_router::Routable;

use crate::components::{
    atoms::struct_hello::StructHello,
    molecules::struct_counter::StructCounter,
    pages::{hello::Hello, home::Home},
};

#[derive(Routable, Clone, PartialEq)]
pub enum Route {
    #[at("/")]
    Home,
    #[at("/hello")]
    Hello,
    #[at("/hello-struct")]
    HelloStruct,
    #[at("/counter")]
    Counter,
}

pub fn switch(route: &Route) -> Html {
    match route {
        Route::Home => html! { <Home /> },
        Route::Hello => html! {<Hello />},
        Route::HelloStruct => html! {<StructHello msg={"Hello from Router"} />},
        Route::Counter => html! {<StructCounter />},
    }
}
