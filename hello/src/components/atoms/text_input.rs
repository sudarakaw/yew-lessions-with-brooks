use wasm_bindgen::JsCast;
use web_sys::HtmlInputElement;
use yew::{function_component, html, Callback, Event, Properties};

#[derive(Properties, PartialEq)]
pub struct Props {
    pub name: String,
    pub handle_on_change: Callback<String>,
}

#[function_component(TextInput)]
pub fn text_input(props: &Props) -> Html {
    let handle_on_change = props.handle_on_change.clone();
    let on_change = Callback::from(move |event: Event| {
        // let target = event.target().unwrap();
        // let input = target.unchecked_into::<HtmlInputElement>();
        let text = event
            .target()
            .unwrap()
            .unchecked_into::<HtmlInputElement>()
            .value();

        handle_on_change.emit(text);
    });

    html! {
        <input type="text" name={props.name.clone()} placeholder={props.name.clone()} onchange={on_change} />
    }
}
