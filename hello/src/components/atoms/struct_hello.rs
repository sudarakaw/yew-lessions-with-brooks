use stylist::{style, Style};
use yew::{html, Component, Properties};

#[derive(Properties, PartialEq)]
pub struct Props {
    pub msg: String,
}

pub struct StructHello {
    pub stylesheet: Style,
}

impl StructHello {
    fn style() -> Style {
        style!(
            r#"
            color: teal;
        "#
        )
        .unwrap()
    }
}

impl Component for StructHello {
    type Message = ();
    type Properties = Props;

    fn create(_ctx: &yew::Context<Self>) -> Self {
        StructHello {
            stylesheet: Self::style(),
        }
    }

    fn view(&self, ctx: &yew::Context<Self>) -> yew::Html {
        html! {
            <h1 class={self.stylesheet.clone()}>{&ctx.props().msg}</h1>
        }
    }
}
