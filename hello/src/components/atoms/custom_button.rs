use yew::{function_component, html, Properties};

#[derive(Properties, PartialEq)]
pub struct Props {
    pub label: String,
}

#[function_component(CustomButton)]
pub fn custom_button(props: &Props) -> Html {
    html! {
        <button>{&props.label}</button>
    }
}
