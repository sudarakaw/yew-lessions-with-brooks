use std::fmt::Display;

use stylist::{style, yew::styled_component};
use yew::{html, Callback, Properties};

#[derive(Properties, PartialEq)]
pub struct Props {
    pub title: String,
    pub color: Color,
    pub on_load: Callback<String>,
}

#[derive(PartialEq)]
pub enum Color {
    Normal,
    Ok,
    Error,
}

impl Display for Color {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let class = match self {
            Self::Normal => "normal",
            Self::Ok => "ok",
            Self::Error => "error",
        };

        write!(f, "{}", class)
    }
}

#[styled_component(MainTitle)]
pub fn main_title(props: &Props) -> Html {
    let stylesheet = style!(
        r#"
        .normal {
            color: white;
        }

        .ok {
            color: green;
        }

        .error {
            color: red;
        }
        "#
    )
    .unwrap();

    props
        .on_load
        .emit(format!("I loaded with \"{}\"", props.title));

    html! {
        <div class={stylesheet}>
            <h1 class={props.color.to_string()}>{&props.title}</h1>
        </div>
    }
}
