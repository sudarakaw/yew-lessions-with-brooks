use yew::{function_component, html};
use yew_router::prelude::Link;

use crate::router::Route;

#[function_component(Home)]
pub fn home() -> Html {
    html! {
        <div>
            <h1>{"HOME!!!"}</h1>

            <nav>
                <ul>
                    <li><Link<Route> to={Route::Hello}>{"To Hello"}</Link<Route>></li>
                    <li><Link<Route> to={Route::HelloStruct}>{"To Struct Hello"}</Link<Route>></li>
                    <li><Link<Route> to={Route::Counter}>{"Counter"}</Link<Route>></li>
                </ul>
            </nav>
        </div>
    }
}
