use yew::{function_component, html, Callback};
use yew_router::prelude::{use_history, History};

use crate::router::Route;

#[function_component(Hello)]
pub fn hello() -> Html {
    let history = use_history().unwrap();
    let on_click = Callback::from(move |_| {
        history.push(Route::Home);
    });

    html! {
        <div>
            <h1>{"Hello!!!"}</h1>

            <button onclick={on_click}>{"Go Home"}</button>
        </div>
    }
}
