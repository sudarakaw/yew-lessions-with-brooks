use yew::{html, Component};

pub enum Msg {
    ButtonClicked,
}

pub struct StructCounter {
    pub count: u32,
}

impl Component for StructCounter {
    type Message = Msg;
    type Properties = ();

    fn create(_ctx: &yew::Context<Self>) -> Self {
        Self { count: 0 }
    }

    fn view(&self, ctx: &yew::Context<Self>) -> yew::Html {
        let button_clicked = ctx.link().callback(|_| Msg::ButtonClicked);

        html! {
            <div>
                <button onclick={button_clicked}>{"Click Me"}</button>
                <p>{"You have cliked "}{self.count}{" times."}</p>
            </div>
        }
    }

    fn update(&mut self, _ctx: &yew::Context<Self>, msg: Self::Message) -> bool {
        match msg {
            Msg::ButtonClicked => {
                self.count += 1;

                true
            }
        }
    }
}
