use std::ops::Deref;

use web_sys::FocusEvent;
use yew::{function_component, html, use_context, use_state, Callback, Properties};

use crate::{
    components::atoms::{custom_button::CustomButton, text_input::TextInput},
    User,
};

#[derive(Default, Clone)]
pub struct State {
    pub username: String,
    pub language: String,
}

#[derive(Properties, PartialEq)]
pub struct Props {
    pub on_submit: Callback<State>,
}

#[function_component(CustomForm)]
pub fn custom_form(props: &Props) -> Html {
    let ctx = use_context::<User>();

    let state = use_state(State::default);

    let cloned_state = state.clone();
    let username_changed = Callback::from(move |username| {
        let mut state = cloned_state.deref().clone();

        state.username = username;

        cloned_state.set(state);
    });

    let cloned_state = state.clone();
    let language_changed = Callback::from(move |language| {
        let mut state = cloned_state.deref().clone();

        state.language = language;

        cloned_state.set(state);
    });

    let cloned_state = state.clone();
    let form_submit = props.on_submit.clone();
    let handle_on_submit = Callback::from(move |event: FocusEvent| {
        event.prevent_default();

        let state = cloned_state.deref().clone();

        form_submit.emit(state);
    });

    html! {
        <form onsubmit={handle_on_submit}>
            <TextInput name="username" handle_on_change={username_changed} />
            <TextInput name="language" handle_on_change={language_changed} />
            <CustomButton label="Submit" />
            <p>{"Username: "}{ctx.clone().unwrap_or_default().username}</p>
            <p>{"Language: "}{ctx.unwrap_or_default().language}</p>
        </form>
    }
}
