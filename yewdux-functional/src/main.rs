use yewdux_functional::App;

fn main() {
    yew::start_app::<App>();
}
