use std::ops::Deref;

use gloo::timers::callback::Timeout;
use web_sys::{Event, FocusEvent, HtmlInputElement};
use yew::{function_component, Callback, TargetCast};
use yew::{html, use_state};
use yewdux::prelude::use_store;

use crate::api;
use crate::store::AuthStore;

#[function_component(LoginForm)]
pub fn login_form() -> Html {
    let (store, auth_dispatch) = use_store::<AuthStore>();

    let onchange_username = {
        let dispatch = auth_dispatch.clone();

        Callback::from(move |event: Event| {
            let username = event.target_unchecked_into::<HtmlInputElement>().value();
            let username = if username.is_empty() {
                None
            } else {
                Some(username)
            };

            dispatch.reduce_mut(|store| store.username = username)
        })
    };

    let onchange_password = auth_dispatch.reduce_mut_callback_with(|store, event: Event| {
        let password = event.target_unchecked_into::<HtmlInputElement>().value();

        store.password = if password.is_empty() {
            None
        } else {
            Some(password)
        };
    });

    let on_submit = {
        let dispatch = auth_dispatch.clone();

        auth_dispatch.reduce_mut_callback_with(move |store, event: FocusEvent| {
            event.prevent_default();

            let username = store.username.clone().unwrap_or_default();
            let password = store.password.clone().unwrap_or_default();
            let dispatch = dispatch.clone();

            wasm_bindgen_futures::spawn_local(async move {
                let response = api::login(username, password).await;

                dispatch.reduce_mut(move |state| state.token = response.token);
            });
        })
    };

    let store = store.deref().clone();
    let token_visible = use_state(|| true);
    let token_visible_clone = token_visible.clone();

    Timeout::new(2000, move || {
        token_visible_clone.set(false);
    })
    .forget();

    html! {
        <form onsubmit={on_submit}>
            <h2>{"Login"}</h2>

            <div>
                <div>
                    <label for="username">{"Username"}</label>
                </div>
                <div>
                    <input type="text" id="username" placeholder="username" onchange={onchange_username} />
                </div>
            </div>

            <div>
                <div>
                    <label for="password">{"Password"}</label>
                </div>
                <div>
                    <input type="password" id="password" placeholder="password" onchange={onchange_password} />
                </div>
            </div>

            <div>
                <button>{"Log In"}</button>
            </div>

            if *token_visible {
            <p>{format!("token: {}", store.token)}</p>
            }
        </form>
    }
}
