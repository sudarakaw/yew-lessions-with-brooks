use serde::{Deserialize, Serialize};
use yewdux::{
    storage::{self, Area},
    store::Store,
};

#[derive(Default, PartialEq, Clone, Serialize, Deserialize)]
pub struct AuthStore {
    pub username: Option<String>,
    pub password: Option<String>,
    pub is_authenticated: bool,
    pub token: String,
}

impl Store for AuthStore {
    fn new() -> Self {
        storage::load(Area::Local)
            .expect("Unable to load state")
            .unwrap_or_default()
    }

    fn should_notify(&self, old: &Self) -> bool {
        storage::save(self, Area::Local).expect("Unable to save state");

        self != old
    }
}
