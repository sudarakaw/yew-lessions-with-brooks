mod api;
mod display_auth;
mod form;
mod store;

use form::LoginForm;
use yew::{function_component, html};

#[function_component(App)]
pub fn view() -> Html {
    html! {
        <div>
            <h1>{"App"}</h1>
            <LoginForm />
        </div>
    }
}
