use reqwasm::http::Request;
use serde::{Deserialize, Serialize};
use serde_json::json;

const BASE_URI: &str = include_str!("uri.txt");

#[derive(Serialize, Deserialize)]
pub struct LoginResponse {
    id: u32,
    username: String,
    pub token: String,
}

#[derive(Serialize, Deserialize)]
struct ApiLoginResponse {
    data: LoginResponse,
}

pub async fn login(username: String, password: String) -> LoginResponse {
    let body = json!({
        "username": username,
        "password":password
    });

    let response = Request::post(&format!("{}/users/login", BASE_URI))
        .header("content-type", "application/json")
        .body(body.to_string())
        .send()
        .await
        .unwrap()
        .json::<ApiLoginResponse>()
        .await
        .unwrap();

    response.data
}
