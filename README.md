# full-stack-todo-rust-course - Code Samples

- [Course Repository](https://github.com/brooks-builds/full-stack-todo-rust-course)
- [Playlist](https://www.youtube.com/playlist?list=PLrmY5pVcnuE_R5qJ0o30eGw77bWmnrUtL)
  by [Brooks Patton](https://twitter.com/Brooks_Patton)
